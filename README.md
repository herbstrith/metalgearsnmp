##  INF01015 Gerência e Aplicações em Redes (2015/2)
 
#  Trabalho Extra classe: Metal Gear SNMP 
 

##  A idéia
 ##
O agente SNMP é inspirado em um robô de uma série de jogos chamada Metal Gear.
O gerente tem controle sobre a posição do robô no mapa, inimigos que o radar do robô detecta, status das partes do robô e informação sobre as armas.

A mib ficou como segue:

-1.MIB Metal Gear

--1.1 SysupTime : counter32 (read only)

--1.2 Position: OctetString (read only)

--1.3 MoveX: integer32

--1.4 MoveY: integer32

--1.5 LookAt: integer32   ---deprecated



--1.6 WeaponTable: table

----- 1.6.x.1 WeaponAmmo: counter32 (read only)

----- 1.6.x.2 WeaponDamage: integer32 (read only)



--1.7 NukeState: integer32   ---deprecated

--1.8 NukeLaunch: counter32  ---deprecated

--1.9 RadarTable: table

----- 1.9.x.1 EnemyPosition: octectString (read only)

----- 1.9.x.2 EnemySize: Counter32 (read only)

----- 1.9.x.3 EnemyThreat: Counter32 (read only)

----- 1.9.x.4 EnemyAttacking: Counter32 (read only)

--1.10 RadarState: Counter32  ---deprecated

--1.11 CameraTable: table   ---deprecated

--1.12 SelectedCamera: Counter32 

--1.13 BodyTable: table

----- 1.13.x.1 BodyPartHealth: Counter32 (read only)

----- 1.13.x.2 BodyPartArmor: Counter32 (read only)

--1.14 selectedTarget: Counter32

--1.15 selectedWeapon: Counter32

--1.16 Attacking: Counter32

--1.17 UnderAttack: Counter32 (read only)



Folhas/nós marcados como deprecated são de features que foram planejadas inicialmente, mas que ou não foram implementadas por questão de deadline ou foram abandonadas / repensadas.


## Ferramentas usadas
 ##

Para o desenvolvimento foi usada a engine de jogos Unity(http://unity3d.com/) para fazer um “simulador” para o robô, bem como o agente e gerente.
Para implementar o protocolo SNMP foi usada a biblioteca SNMPSHARPNET ( http://www.snmpsharpnet.com/), usando o padrão SNMP V1.
As imagens usadas para a GUI bem como o modelo 3D Metal Gear são oriundos de diversos sites da internet.

## Sobre a implementação
 ##

Existe uma thread que executa a engine unity tanto no agente como no gerente.
No agente, temos duas threads a mais. Umathread para tratar das requisições SNMP e outra thread para a geração de traps.
No gerente, temos uma thread a mais para escutar as traps.



## Como executar
 ##

O download do projeto pode ser feito em https://bitbucket.org/herbstrith/metalgearsnmp/

A pasta Assets contem o código e assets usados para a criação.

A pasta Agent_build, contém o executável do agente em sua raiz (agent.exe).

A pasta Manager_build contém o executável do agente em sua raiz (manager.exe).

É usada a porta 16100 para as requisições SNMP e a porta 16009 para as traps.


## Principais problemas
 ##

A engine Unity não é muito amigável com o uso de threads. Erros ou problemas de código que acontecem nas threads que não a Main não são informados e a thread apenas morre. Isto se provou um grande empecilho, dificultando o processo de implementação e debug.